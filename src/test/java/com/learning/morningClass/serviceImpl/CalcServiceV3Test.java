package com.learning.morningClass.serviceImpl;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.learning.morningClass.servicesImpl.CalcServiceV3;

@Test
public class CalcServiceV3Test {

	/*
	 * happy path, should return sum of entered number
	 * */
	public void add_success() {
		CalcServiceV3 calc = new CalcServiceV3();
		final int NUM1 = 3;
		final int NUM2 = 5;
		final int SUM_EXPECTED = 8;

		assertEquals(calc.add(NUM1, NUM2), SUM_EXPECTED, "method add() should return "+SUM_EXPECTED+" given numbers "+NUM1+" and "+NUM2);

	}

}
