package com.learning.morningClass.serviceImpl;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.learning.morningClass.servicesImpl.PasswordValidator;


@Test
public class PasswordValidatorTest {

	final int MIN_LENGTH = 8;
	
	//it must be of 8 char length
	public void shouldHaveMinLenght() {
		final String password ="PassTest";
		PasswordValidator validator =  new PasswordValidator(password);
		assertTrue(validator.hasMinLength(MIN_LENGTH),"hasMinLength() method should return true given password "+password+" of length "+MIN_LENGTH);
	}
	
	// it must contain _
	public void shouldContainSpecialCharacter() {
		final String password ="PassTest_passw";
		PasswordValidator validator =  new PasswordValidator(password);
		assertTrue(validator.hasSpecialCharacters(),"hasSpecialCharacters() should return true if password "+password+" has special characters");
	}
	//it must contain digits
	//it must contain uppercase 
	//it must contain lowercase
	// valid password
	public void validPassword_success() {
		final String password ="PassTest_passw";
		
		PasswordValidator validator =  new PasswordValidator(password);
		assertTrue(validator.validate(MIN_LENGTH),"password with all requirements should pass");
	}
}
