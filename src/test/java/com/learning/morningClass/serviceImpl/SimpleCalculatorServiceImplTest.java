package com.learning.morningClass.serviceImpl;



import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.testng.annotations.DataProvider;

import com.learning.morningClass.servicesImpl.SimpleCalculatorServiceImpl;

public class SimpleCalculatorServiceImplTest {

	SimpleCalculatorServiceImpl service = new SimpleCalculatorServiceImpl();

	@Test
	public void add_success() {
		int expectedResult = 6;
		int actualResult = service.add(new int[] { 1, 2, 3 });
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void add_empty() {
		int actual = service.add(new int[] {});
		assertEquals(0, actual);
	}

	@Test
	public void add_oneValue() {
		assertEquals(3, service.add(new int[] { 3 }));
	}

	@Test
	public void subtract_success() {
		int expectedResult = 4;
		int actualResult = service.subtract(5, 1);
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void subtract_zero() {
		assertEquals(4, service.subtract(4, 0));
	}
}
