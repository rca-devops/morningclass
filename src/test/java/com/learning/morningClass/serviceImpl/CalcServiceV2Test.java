package com.learning.morningClass.serviceImpl;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.learning.morningClass.servicesImpl.CalcServiceV2;

@Test
public class CalcServiceV2Test {

	/*
	 * Testing happy path
	 * 
	 * */
	public void add_success() {
		final int NUM1 = 4;
		final int NUM2 = 2;
		final int SUM_EXPECTED = 6;
		CalcServiceV2 calc = new CalcServiceV2();

		assertEquals(calc.add(NUM1, NUM2), SUM_EXPECTED,
				"add method should return " + SUM_EXPECTED + " when given " + NUM1 + " and " + NUM2);
	}
}
