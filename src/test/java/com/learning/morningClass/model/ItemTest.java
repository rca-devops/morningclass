package com.learning.morningClass.model;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.learning.morningClass.models.Item;

@Test
public class ItemTest {

	public void value_success() {
		Item item =  new Item(1, "Car", 500, 6);
		assertEquals(item.getValue(), 3000);
	}
}
