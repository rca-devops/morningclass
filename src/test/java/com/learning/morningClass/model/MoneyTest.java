package com.learning.morningClass.model;


import static org.junit.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.learning.morningClass.models.Money;

@Test
public class MoneyTest {

	@DataProvider
	private static final Object[][] getMoney() {
		return new Object[][] { { 10, "USD" }, { 20, "EUR" } };
	}

	@Test(dataProvider = "getMoney")
	public void constructorShouldSetAmountAndCurrency_dataprovider(int amount, String currency) {
		Money money = new Money(amount, currency);
		assertEquals(money.getAmount(), amount);
		assertEquals(money.getCurrency(), currency);
	}

	@Test
	public void constructorShouldSetAmountAndCurrency() {
		Money money = new Money(10, "USD");
		assertEquals(10, money.getAmount());
		assertEquals("USD", money.getCurrency());
	}
}
