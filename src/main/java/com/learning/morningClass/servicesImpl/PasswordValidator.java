package com.learning.morningClass.servicesImpl;

public class PasswordValidator {

	private String password;

	public PasswordValidator(String password) {
		this.password = password;

	}

	public boolean hasMinLength(int minLenght) {
		if (password.length() >= minLenght)
			return true;
		return false;
	}

	public boolean hasSpecialCharacters() {
		if (password.matches(".*[!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?].*"))
			return true;
		return false;
	}

	public boolean validate(int minLength) {
		if(hasMinLength(minLength)&& hasSpecialCharacters()) return true;
		return false;
	}

}
