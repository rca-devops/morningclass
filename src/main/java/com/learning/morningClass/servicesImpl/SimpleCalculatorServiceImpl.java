package com.learning.morningClass.servicesImpl;

import com.learning.morningClass.services.SimpleCalculatorService;

public class SimpleCalculatorServiceImpl implements SimpleCalculatorService {

	@Override
	public int add(int[] data) {
		int sum = 0;
		for (int num : data) {
			sum += num;
		}
		return sum;
	}

	@Override
	public int subtract(int num1, int num2) {

		return num1 - num2;
	}

	@Override
	public int multiply(int[] data) {
		int prod = 0;
		for (int num : data) {
			prod = prod * num;
		}
		return prod;
	}

}
