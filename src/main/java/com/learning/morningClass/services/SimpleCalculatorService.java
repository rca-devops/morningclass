package com.learning.morningClass.services;

public interface SimpleCalculatorService {
	
	int add(int[] data);
	
	int subtract(int num1, int num2);
	
	int multiply(int[] data);

}
