package com.learning.morningClass.models;

public class Item {
	
	private long id;
	
	private String name;
	
	private double unitPrice;
	
	private int quantity;
	
	private double value;
	
	
	public Item() {
		super();
	}

	public Item(long id, String name, double unitPrice, int quantity) {
		super();
		this.id = id;
		this.name = name;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
	}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getValue() {
		return value;
	}

	public void setValue() {
		this.value = this.unitPrice* this.quantity;
	}

	
	
	

}
