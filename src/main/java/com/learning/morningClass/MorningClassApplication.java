package com.learning.morningClass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MorningClassApplication {

	public static void main(String[] args) {
		SpringApplication.run(MorningClassApplication.class, args);
	}

}
